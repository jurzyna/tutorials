function changePowerTotal( totalPower, genId, genStatus, power){
  if(genStatus == "on"){
  	totalPower += power;
  	alert("Generator #" + genId + " is now on, adding " + power + " MW, for a total of " + totalPower + " MW!");
  } else if(genStatus == "off"){
  	totalPower -= power;
  	alert("Generator #" + genId + " is now off, removing " + power + " MW, for a total of " + totalPower + " MW!");
  }
  return totalPower;
}