var totalGen = 19;
var totalMW = 0;
var genProduce = 0;
for(var currentGen = 1; currentGen <= totalGen; currentGen++){
  if(currentGen % 2 == 0 && currentGen <=4){ 
      genProduce = 62;
      totalMW += genProduce;
      console.log("Generator #"+currentGen+" is on, adding "+genProduce+" MW, for a total of "+totalMW+" MW!");
  } else   if(currentGen % 2 == 0 && currentGen >=5){
    	genProduce = 124;
      totalMW += genProduce;
      console.log("Generator #"+currentGen+" is on, adding "+genProduce+" MW, for a total of "+totalMW+" MW!");
  } else{
  		console.log("Generator #" + currentGen + " is off.");
  }
}