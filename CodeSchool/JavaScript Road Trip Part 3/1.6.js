var numbers = [12, 4, 3, 9, 8, 6, 10, 1];
var results = numbers.map( function (arrayCell) { return arrayCell * 2; } );


// Substracting passangers 2 level array //
var passengers = [ ["Thomas", "Meeks"],
                   ["Gregg", "Pollack"],
                   ["Christine", "Wong"],
                   ["Dan", "McGaw"] ];
var modifiedNames = passengers.map(function(arrayCell) {
  return arrayCell[0] + "q " + arrayCell[1];
});

//Alerting message from the modifiedNames array //
var modifiedNames = [ "Thomas Meeks",
                      "Gregg Pollack",
                      "Christine Wong",
                      "Dan McGaw" ];
modifiedNames.map(function(name) {
  alert("Yo, " + name + "!"); 
});

// Building an array with functions //
var puzzlers = [
	function(input) {
    return (input * 3) - 8;
  },
  function(input) {
    return (input + 2) * (input + 2) * (input + 2);
  },
  function(input) {
    return (input * input) - 9;
  },
   function(input) {
		 return input % 4;
  },
];