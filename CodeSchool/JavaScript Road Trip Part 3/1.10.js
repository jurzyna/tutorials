var parkRides = [ ["Birch Bumpers", 40] , ["Pines Plunge", 55]
["Cedar Coaster", 20] , ["Ferris Wheel of Firs", 90] ];

var fastPassQueue = [ "Cedar Coaster", "Pines Plunge", "Birch Bumpers", "Pines Plunge" ];

function buildTicket ( allRides, passRides, pick ) {
	if(passRides[0] == pick){
		var pass = passRides.shift();
		return function () { 
			alert("Quick! You've got a Fast Pass to " + pass + "!");
		};
	} else {
		for(var i = 0; i<allRides.length; i++){
			if(allRides[i][0] == pick){
				return function () { 
					alert("A ticket is printing for " +pick + "!\n" + "Your wait time is about " + allRides[i][1] + " minutes.");
				};
			}
		}
	}
}

var wantsRide = "Birch Bumpers";

// Stores the return function in a variable
var ticket = buildTicket( parkRides, fastPassQueue, wantsRide );
ticket();

// Imedietly call the function when ut us returned
	buildTicket( parkRides, fastPassQueue, wantsRide ); // only the function itself will be returned here
	buildTicket( parkRides, fastPassQueue, wantsRide )() // invokes the function inside the function;

	// build applyAndEmpty function expression here
var applyAndEmpty = function (input, queue) {
  var length = queue.length;
  for (var i = 0; i < length; i++) {
    var function_returned = queue.shift();
    var result = function_returned(input);
    input = result;
  }
  return input;
};

alert(applyAndEmpty(start, puzzlers));