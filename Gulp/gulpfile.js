var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	sass = require('gulp-ruby-sass'),
	plumber = require('gulp-plumber');

gulp.task('scripts', function(){
	gulp.src('src/js/*.js')
		.pipe(plumber())
	 	.pipe(uglify())
	 	.pipe(gulp.dest('build/js'));
});

gulp.task('styles', function(){
	return sass('src/scss/**/*.scss')
		.on('error', sass.logError)
		.pipe(plumber())
		.pipe(gulp.dest('build/css/'));
});

gulp.task('watch', function() {
	gulp.watch('src/js/*.js', ['scripts']);
	gulp.watch('src/scss/**/*.scss', ['styles']);
});

gulp.task('default', ['scripts', 'styles', 'watch']);